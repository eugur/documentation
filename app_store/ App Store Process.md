# # 7. App Store Process
## Certificates are private and public key pairs. It is possible to use a single certificate for signing multiple apps. It is needed to have separate provisioning profile for every app because every app has a unique bundle id.

# Identifier (Bundle id)
Identifier is unique for every app and it is used to identify your app in App Store.

# App ID
App ID is a unique identifier that identifies your app in a provisioning profile.
There are two types of App IDs
	* App ID used for a single app: These id’s specific to the application. It can only be used for that specific app.
	* App ID used for a set of apps - Wildcard App ID: This id can be used for multiple apps. It is needed to use * as the last digit in the Bundle Id field. 
App capabilities are enabled for an App ID serve as a whitelist of the capabilities such as Push Notifications, In App Purchase, Game Center etc.

For the explicit App ID, enter the app's bundle ID in the bundle ID field. 
 Important 
The explicit App Id you enter should match the bundle ID you entered.

How to register for an App ID?  
_Note: App ID creation process can be updated periodically by Apple. So, these screenshots or process flow may  bit different than explained below._ 

**Step 1:**   Account on menu and log in with your Apple Developer account.

**Step 2:** Click Certificates, Identifiers & Profiles

![appId_ss1](appId_ss1.png)

**Step 3:** In Certificates, Identifiers & Profiles, after identifiers is selected, click the add (+) button next to Identifiers Header. 

![appId_ss2](appId_ss2.png)

**Step 4:** Select App IDs and click continue.

![appId_ss3](appId_ss3.png)

**Step 5:** Enter [App ID](#App-ID) description and add bundle id for your specific application. You can also select capabilities for your app listed below and then click continue. (App capabilities can be updated any time in Apple's developer portal or inside the project in Xcode settings)

![appId_ss4](appId_ss4.png)

**Step 6:** Click continue button. For the development it is not needed to give extra app detail in this phase.

![appId_ss5](appId_ss5.png)

**Step 7:** Click register button after you've checked your [App ID](#App-ID) details.

![appId_ss6](appId_ss6.png)

# Provisioning Profile
Provisioning Profile is link between the device and the developer account. It is a combination of your [App ID](#App-ID) and your Distribution Certificates. It allows and authorizes your app to use particular services like Push Notifications, In App Purchase, Game Center etc.

 [Apple's definition:](https://developer.apple.com/documentation/appstoreconnectapi/profiles)  The profiles resource represents the [provisioning profiles](#Provisioning-Profile) that allow you to install apps on your iOS devices or Mac. 

Provisioning Profile contains: 
+ [Development Certificates](#Development-Certificate)
+ Unique device [identifier](#Identifier-(Bundle-id)) (List of the devices that the app can run on)+ App ID

### There are two types of Provisioning Profiles.
+ Development Profiles (This profile must be installed on each device which you want to run your app. )
+ Distribution Profiles (These certificates don't specifies any Device ID.)


# Certificates

There are two types of certificates which are Development and Distribution. It is needed to have Mac and create a signing request from a Mac. 
In order to do that, open Keychain Access, then from top left drop down menu, select _Certificate Assistan > Request a Certificate From a Certificate Authority._ 
Enter the organizations id and name to create the request file and save it on your disk. 
Open your developer account to create the certificate. 

# Development Certificate
Development Certificate is needed to sign (run) the application and to take advantage of advanced capabilities such as CloudKit and Push Notifications etc.

# Distribution Certificate
Distribution certificate identifies your team/organization with related [provisioning profile](#Provisioning-Profile) (distribution) and enables you submit your app to AppStore.
Distribution certificate extension is .p12 format. 

# How the system works?
When the developer run the app on a device, the [Provisioning Profile](#Provisioning-Profile) in the Mac checks the developer [certificate](#Certificate) stored in Mac’s key chain. 
This [certificate](#Certificate) used to sign the code by Xcode.
Device UUID is checked with the UUID specified in the [Provisioning Profile](#Provisioning-Profile).
Then [App ID](#App-ID) in the [provisioning profile](#Provisioning-Profile) is matched with the bundle id in the app.
The entitlements (app capabilities) are checked whether they match with the bundle id or not. 
Lastly private key ([distribution certificate](#Distribution-Certificate)) used to sign the app matches the public key in the certificate.

# Apple Developer Portal & iTunes Connect Member Management
**Step 1:** Open [Apple Developer Portal](developer.apple.com) and click People
(iTunesConnect Users and Apple Developer Portal Useres are directs to the same [page](https://appstoreconnect.apple.com/access/developers).)

![appId_ss1](appId_ss1.png)

**Step 2:** Click Manage Users

![manage_users_1](manage_users_1.png)

**Step 3:** Click + button to add a new user.

![manage_users_2](manage_users_2.png)

You can give developer or admin role. If you select developer as role, you can also select specific apps to give permission. 

# App Archive & .ipa

It is needed to archive app to distritube. 

**Step 1:** Select your App Target from schema list and selecte "Generic iOS Device" from device selection menu. 
**Step 2:** From Xcode Menu, select *Product* > *Archive*. Organizer window will be open with the list of your apps on the left side. Your app and the version you want to archive comes already selected.

![arhive_ss1](archive_ss1.png)


**Step 3:** To start upload process, Click *Distribute App* button. 

![archive_ss2](archive_ss2.png)

## Upload to App Store

**Step 1:** Select a distribution method. To upload your app to the App Store, you have to select *App Store Connect* option. Then click *Next*.

![archive_ss3](archive_ss3.png)

**Step 2:** Select *Upload* option to send your app to App Store Connect. Click *Next* and start uploading process.

![archive_ss4](archive_ss4.png)
![archive_ss5](archive_ss5.png)

When the uploading is completed successfully, you will be notified. 

![archive_ss6](archive_ss6.png)

## ipa File
**ipa** *(iOS App Store Package)* file is an iOS application archive file. It includes binary version of an complete iOS app and can be uncompressed editing its extension to .zip. An ipa file is created and uploaded to the App Store Connect when archive process is completed. 

# App Store Review

Uploaded apps is listed in [App Store Connect](https://appstoreconnect.apple.com).

You can also find Apple's Development, Design and Brand & Marketing Guidelines in [Apple Developer Platform](https://developer.apple.com/app-store/review/guidelines/).


**Step1:** Select your app.

![appStoreReview_ss1](appStoreReview_ss1.png)

**Step2:** Fill required fields.

![appStoreReview_ss2](appStoreReview_ss2.png)

**Step3:** Select the build version you want to submit to the App Store. Build list is under *Activity* tab.

![appStoreReview_ss3](appStoreReview_ss3.png)
![appStoreReview_ss4](appStoreReview_ss4.png)
![appStoreReview_ss5](appStoreReview_ss5.png)

**Step4:** After all required information filled, click *Submit* button.

![appStoreReview_ss6](appStoreReview_ss6.png)
